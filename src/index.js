
/*

Contexto:
El equipo debe desarrollar una app que maneje el funcionamiento de una biblioteca.

Actividad 1

1. Cree un archivo json que contenga un listado de libros, este debe contener el nombre, autor, clasificación (clasificar por temas. Por ejemplo: Las crónicas de una muerte anunciada es una novela corta)
2. Cree una función que imprima todos los libros de la biblioteca.
3. Cree una función que permita buscar un libro por su autor.
4. Cree una función que permita agregar un libro nuevo.
5. Cree una función que muestre los libros de una clasificación específica.

*/
var fs = require('fs');
const path = require('path');

function monstrarLibros(){
    let jsonBooks= require(path.join(__dirname, 'libros.json')); 
    for (const books of jsonBooks['libros']) {
        let {nombre, autor, clasificacion} = books
        console.log(`Nombre: ${nombre}\nAutor: ${autor}\nClasificacion:${clasificacion}\n`)
    }
}

function buscarLibro(author){
    let jsonBooks= require(path.join(__dirname, 'libros.json'));
    let foundBook = false;
    let countBooks = 0;
    for(let books of jsonBooks['libros']){
        let {nombre, autor, clasificacion} = books
        if(autor == author){
            foundBook = true
            countBooks +=1
                
            }
        }
    if (foundBook){
        (countBooks >1) ? console.log(`Hay ${countBooks} libros del autor ${author}`) : console.log(`Hay ${countBooks} libro del autor ${author}`)
            
    }else{
        console.log(`No Hay libros del autor ${author}`)
    }
}

function agregarLibro(nombre,autor,clasificacion){
    let jsonBooks= require(path.join(__dirname, 'libros.json'));
    let libro = { nombre, autor, clasificacion}
    jsonBooks["libros"].push(libro);
    const data = JSON.stringify(jsonBooks);

    /*
    fs.writeFile(path.join(__dirname, 'libros.json'), data, (err) => {    
        if (err) throw err;
        c
        onsole.log('Data written to file');
    });*/
}

function filtrarLibros(clasi){
    let jsonBooks= require(path.join(__dirname, 'libros.json'));
    for(let books of jsonBooks['libros']){
        let {nombre, autor, clasificacion} = books
        if(clasificacion == clasi){
            console.log(`Nombre: ${nombre}\nAutor: ${autor}\nClasificacion:${clasificacion}\n`)
        }
    }
}

//monstrarLibros()
//buscarLibro('KELLEN, ALICE')
//agregarLibro("23 Otoños Antes De Ti","KELLEN, ALICE","3")
//monstrarLibros()
filtrarLibros(3)